#!/usr/bin/env bash
Green="\033[32m"
Font="\033[0m"
Red="\033[31m"
Yellow="\033[33m" 

#root权限
root_need(){
    if [[ $EUID -ne 0 ]]; then
        echo -e "${Red}Error:This script must be run as root!${Font}"
        exit 1
    fi
}

#检测ovz
ovz_no(){
    if [[ -d "/proc/vz" ]]; then
        echo -e "${Red}Your VPS is based on OpenVZ，not supported!${Font}"
        exit 1
    fi
}

add_swap(){
echo -e "${Green}请输入需要添加的swap，建议为内存的2倍！${Font}"
read -p "请输入swap数值(例：1024的倍数):" swapsize

#检查是否存在swapfile
grep -q "swapfile" /etc/fstab

#如果不存在将为其创建swap
if [ $? -ne 0 ]; then
	echo -e "${Green}swapfile未发现，正在为其创建swapfile${Font}"
    echo -e "${Yellow}请选择创建swap文件的方式${Font}"
    echo -e "${Yellow}>> 1 << DD创建[通用，但时间较长]${Font}"
    echo -e "${Yellow}>> 2 << fallocate创建[很快，但不支持centos]${Font}"
    read -p "请选择创建文件的方式:" add_swap_choice
    case $add_swap_choice in
    2)
    fallocate -l ${swapsize}M /swapfile
    ;;
    1)
    dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} 
    ;;
    *)
    echo -e "输入错误！请重试！"
    add_swap
    ;;
    esac
	chmod 600 /swapfile
	mkswap /swapfile
	swapon /swapfile
	echo '/swapfile none swap defaults 0 0' >> /etc/fstab
         echo -e "${Green}swap创建成功，并查看信息：${Font}"
         cat /proc/swaps
         cat /proc/meminfo | grep Swap
else
	echo -e "${Red}swapfile已存在，swap设置失败，请先运行脚本删除swap后重新设置！${Font}"
fi
}

del_swap(){
#检查是否存在swapfile
grep -q "swapfile" /etc/fstab

#如果存在就将其移除
if [ $? -eq 0 ]; then
	echo -e "${Green}swapfile已发现，正在将其移除...${Font}"
	sed -i '/swapfile/d' /etc/fstab
	echo "3" > /proc/sys/vm/drop_caches
	swapoff -a
	rm -f /swapfile
    echo -e "${Green}swap已删除！${Font}"
else
	echo -e "${Red}swapfile未发现，swap删除失败！${Font}"
fi
}

change_swap(){
#检查是否存在swapfile
grep -q "swapfile" /etc/fstab

#如果存在就允许修改
if [ $? -eq 0 ];then
    echo -e "${Green}这边是一些swap分区占比的建议${Font}"
    echo -e "${Red}> 0 < 系统尽量避免使用swap分区${Font}"
    echo -e "${Red}> 10-20 < 系统在一定程度上使用swap分区${Font}"
    echo -e "${Red}> 60-100 < 系统更积极地使用swap分区${Font}"
    read -p "请输入具体的数值(0-100)：" swappiness_choice
        sudo sysctl vm.swappiness=${swappiness_choice}
        sudo sysctl -p
        echo -e "${Red}设置成功！ ${Font}"
else
   echo -e "${Red}swapfile未发现，swap修改失败！${Font}"
fi
}


red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
    echo -e "\033[34m\033[01m$1\033[0m"
}

#开始菜单
main(){
root_need
ovz_no
clear
   while true; do
red     "================================================"
green   "||  ____    _____  _______  _________  _______||"
yellow  "|| / __ \  /  _/ |/ / ___/ / __/ __/ |/ / ___/||"
blue    "||/ /_/ / _/ //    / (_ / / _// _//    / (_ / ||"
yellow  "||\___\_\/___/_/|_/\___/ /_/ /___/_/|_/\___/  ||"
green   "||                                            ||"
red     "================================================"
blue    "||            >>>>> 清风徐来 <<<<<             ||"
red     "================================================"
#echo -e "======================================="
echo -e "${Green}Linux VPS一键swap脚本${Font}"
echo -e "${Green}1、添加swap${Font}"
echo -e "${Green}2、删除swap${Font}"
echo -e "${Green}3、修改swap分区占比${Font}"
echo -e "${Green}4、退出${Font}"
red     "================================================"

read -p "请输入数字 [1-4]:" num

case "$num" in
    1)
    add_swap
    ;;
    2)
    del_swap
    ;;
    3)
    change_swap
    ;;
    4)
    exit 1
    ;;
    *)
    clear
    echo -e "${Green}请输入正确数字 [1-2]${Font}"
    main
    ;;
    esac
done
}
main