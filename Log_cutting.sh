[root@nginx-web script]# cat nginx_log.sh
#!/bin/bash
date=`date +%F -d -1day`
log_dir=/var/log/nginx/
log_name=access.log
[ -d $log_dir ] && cd $log_dir || exit 1
[ -f $log_name ] || exit 1
/bin/mv $log_name $log_name.${date}
/usr/sbin/nginx -s reload
tar czf $log_name.${date}.tar.gz $log_name.${date} && rm -rf $log_name_${date}


#delete
cd $log_dir || exit 1
find ./ -mtime +7 -type f -name *.log | xargs rm -rf