#!/bin/bash
#本脚本用于系统工具
#对时，更改IP，安装sway分区，下载常用软件
#author: qingfeng
#creation_time:2023

#1、关闭防火墙及安装环境/更改IP/开启DNS/对时/安装swap分区
DEV_DEPLOYMENT(){
    read -p "请选择需要的服务：
    1.关闭防火墙/安装环境
    2.更改IP
    3.开启DNS
    4.对时
    5.安装swap分区：" choice   
    case $choice in
        1)
            #关闭防火墙及安装环境
            systemctl stop firewalld;systemctl disable firewalld;
            red "防火墙已关闭,关闭开机自启服务！"
            setenforce 0
            sed -i 's/^\(SELINUX=.*\)/SELINUX=disabled/' /etc/selinux/config
            red "selinux已关闭，已关闭开机自启服务！"
            wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
            wget -O /etc/yum.repos.d/epel.repo https://mirrors.aliyun.com/repo/epel-7.repo
            red "Centos-7 epel-7 已更换为阿里的源"
            yum -y install vim lrzsz net-tools wget htop lsof elinks psmisc tree unzip
            red "安装--vim lrzsz net-tools wget htop lsof elinks psmisc tree unzip--成功"
        ;;
        2)
            #更改IP
            read -p "是否已经配置网卡服务[y/n]：" yn
            if [ $yn = n ];then
            echo "
            IPADDR=192.168.116.111
            NETMASK=255.255.255.0
            GATEWAY=192.168.116.2
            DNS1=192.168.116.2
            以上为默认格式，仅供参考！"        
                ed -ie 's/BOOTPROTO=dhcp/BOOTPROTO=static/;s/ONBOOT=no/ONBOOT=yes/' /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入你设置的IP地址：" ip
                sed -i "$ a IPADDR=$ip"  /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入掩码：" net
                sed -i "$ a NETMASK=$net" /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入网关：" way
                sed -i "$ a GATEWAY=$way" /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入DNS：" dns
                sed -i "$ a DNS1=$dns" /etc/sysconfig/network-scripts/ifcfg-ens33
                    red "网卡配置成功！"
            else
                sed -i '16,19 s/\(.*\)/#\1/'  /etc/sysconfig/network-scripts/ifcfg-ens33
                    echo "注意，16-19段已被注释！！！"
                read -p "是否需要重新配置网卡服务【y/n】：" YN
                if [ $YN = y ];then
                echo "
                IPADDR=192.168.116.111
                NETMASK=255.255.255.0
                GATEWAY=192.168.116.2
                DNS1=192.168.116.2
                以上为默认格式，仅供参考！"        
                sed -ie 's/BOOTPROTO=dhcp/BOOTPROTO=static/;s/ONBOOT=no/ONBOOT=yes/' /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入你设置的IP地址：" ipp
                sed -i "$ a IPADDR=$ipp"  /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入掩码：" nett
                sed -i "$ a NETMASK=$nett" /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入网关：" wayy
                sed -i "$ a GATEWAY=$wayy" /etc/sysconfig/network-scripts/ifcfg-ens33
                    read -p "请输入DNS：" dnss
                sed -i "$ a DNS1=$dnss"  /etc/sysconfig/network-scripts/ifcfg-ens33  
                    red "网卡配置成功！"
                fi
            fi  
            ;;
            3)
                #开启DNS
                sed -i '115s/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config;systemctl restart sshd
                red "DNS已关闭！！！"
            ;;
            4)
                #对时
                # 关闭防火墙
                systemctl stop firewalld
                setenforce 0
                # 下载软件
                yum -y instal ntpdate
                # 查看当前时区
                timedatectl
                # 列出可用时区
                #timedatectl list-timezones
                # 修改上海时区
                timedatectl set-timezone Asia/Shanghai
                # 验证修改是否生效
                timedatectl
                # 对时
                ntpdate ntp.aliyun.com
                red "对时成功，当前时间为：" 
                date +'%F %H:%M:%S'
            ;;
            5)
                #安装swap分区
                bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/swap.sh)
            ;;
    esac        
}


#2、服务部署
SERVICE_DEPLOYMENT(){
    read -p "请选择要安装的软件：1.Nginx 2.MySQL 3.docker 4.K8S：" choice
    
    case $choice in
        1)  
            #下载nginx
            echo "[nginx-stable]" >> /etc/yum.repos.d/nginx.repo
            echo 'name=nginx stable repo' >> /etc/yum.repos.d/nginx.repo
            echo 'baseurl=http://nginx.org/packages/centos/$releasever/$basearch/' >> /etc/yum.repos.d/nginx.repo
            echo 'gpgcheck=0' >> /etc/yum.repos.d/nginx.repo
            echo 'enabled=1' >> /etc/yum.repos.d/nginx.repo
            yum -y install nginx
            systemctl start nginx && systemctl enable nginx
            ;;
        2)
            #下载数据库MySQL--调用脚本
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/MySQL_install.sh)
            ;;
        3)
            #下载docker
            # 卸载旧版本
            yum remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine
            # 需要的安装包
            yum install -y yum-utils
            # 设置镜像仓库
            yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
            # 更新yum软件包索引
            yum makecache fast
            # 下载依赖包
            yum -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-compose
            # 启动docker
            systemctl start docker
            #配置镜像加速器
            mkdir -p /etc/docker
            tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://8zc4xpn2.mirror.aliyuncs.com"]
}
EOF
            #重启镜像
            systemctl daemon-reload
            systemctl restart docker
            ;;
        4)
            # K8S一键脚本安装
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/k8s-install.sh)
            ;;
        *)
            echo "无效的选择"
            ;;
    esac
}

#3、中间件部署
MIDDLEWARE_DEPLOYMENT(){
    bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/dev/rear-end-package.sh)
}

#4、微服务部署
Microservice_DEPLOYMENT(){
    read -p "请选择要安装的服务：1.nexus3 2.nacos 3.gitlab 4.docker 5.安装Elasticsearch 7.17.10  6.安装Kibana 7.17.10 ：" choice

    case $choice in
        1)
            # nexus3 部署
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/nexus3.sh)
            ;;
        2)
            # nacos 部署
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/nacos.sh)
            ;;
        3)
            # gitlab 部署
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/gitlab.sh)
            ;;
        4)
            # docker 部署
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/docke-install.sh)
            ;;
        5)
            # elasticsearch 部署
            bash <(curl https://gitee.com/xbd666/qingfeng/raw/master/es.sh)
            ;;
        6)
            # kibana 部署
            bash <(curl https://gitee.com/xbd666/qingfeng/raw/master/kibana.sh)
            ;;
        *)
           echo "无效的选择"
            ;;
    esac 
}

#4、备份
Backup_copy(){
    read -p "请选择要安装的软件：1.数据备份 2.日志切割：" choice

     case $choice in
        1)  
            # 数据备份脚本
            bash <(curl -sSL https://gitee.com/xbd666/qingfeng/raw/master/Backup_copy.sh)
            ;;
        2)
            #日志切割
            date=`date +%F -d -1day`
            log_dir=/var/log/nginx/
            log_name=access.log
            [ -d $log_dir ] && cd $log_dir || exit 1
            [ -f $log_name ] || exit 1
            /bin/mv $log_name $log_name.${date}
            /usr/sbin/nginx -s reload
            tar czf $log_name.${date}.tar.gz $log_name.${date} && rm -rf $log_name_${date}
            #delete
            cd $log_dir || exit 1
            find ./ -mtime +7 -type f -name *.log | xargs rm -rf
            ;;
        *)
          echo "无效的选择"
            ;;
    esac  
}

#退出
exit_menu(){
    green "再见！！！"
    break
}

red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
    echo -e "\033[34m\033[01m$1\033[0m"
}

# 主菜单选择
main_menu() {
    while true; do
red      "==============================================="
green   "||  ____    _____  _______  _________  _______||"
yellow  "|| / __ \  /  _/ |/ / ___/ / __/ __/ |/ / ___/||"
blue    "||/ /_/ / _/ //    / (_ / / _// _//    / (_ / ||"
yellow  "||\___\_\/___/_/|_/\___/ /_/ /___/_/|_/\___/  ||"
green   "||                                            ||"
red     "==============================================="
blue    "||            >>>>> 清风徐来 <<<<<            ||"
red     "==============================================="
echo    "1) 环境部署/安装swap分区"
echo    "2) 服务部署"
echo    "3) 中间件部署"
echo    "4) 微服务部署"
echo    "5) 备份"
echo    "6) 退出"
red     "==============================================="

read -p "请输入选项： " select
        case $select in
            1)
                DEV_DEPLOYMENT
                ;;
            2)
                SERVICE_DEPLOYMENT
                ;;
            3)
                MIDDLEWARE_DEPLOYMENT
                ;;
            4)
                Microservice_DEPLOYMENT
                ;;
            5)
                Backup_copy
                ;;
            6)
                exit_menu
                ;;
            *)
                red "无效的选择，请重新输入"
                continue 2
                ;;
        esac

        echo "============================"
        read -p "按任意键返回主菜单" any_key
        clear
    done
}

#运行脚本
main_menu
