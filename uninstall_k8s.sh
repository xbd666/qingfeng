#!/bin/bash
# 脚本功能：卸载k8s集群和相关环境
# 作者：Mr.xu
# 时间：2024-11-29

# 定义颜色
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

echo -e "${RED}开始卸载 Kubernetes 集群和相关组件...${NC}"

# 停止 kubelet 和 docker 服务
echo -e "${YELLOW}停止kubelet和docker服务...${NC}"
systemctl stop kubelet
systemctl stop docker

# 重置 Kubernetes 集群
echo -e "${YELLOW}重置Kubernetes集群...${NC}"
kubeadm reset -f
rm -rf ~/.kube

# 删除 CNI 网络配置
echo -e "${YELLOW}删除CNI网络配置...${NC}"
rm -rf /etc/cni/net.d
rm -rf /var/lib/cni/

# 删除 Kubernetes 软件包和相关配置
echo -e "${YELLOW}删除Kubernetes软件包和配置...${NC}"
yum remove -y kubelet kubeadm kubectl kubernetes-cni

# 删除 Docker 容器和镜像
echo -e "${YELLOW}删除Docker容器和镜像...${NC}"
docker rm -f $(docker ps -aq) || echo "没有运行的Docker容器需要删除"
docker rmi -f $(docker images -aq) || echo "没有Docker镜像需要删除"

# 删除 Docker 数据目录
echo -e "${YELLOW}删除Docker数据目录...${NC}"
read -p "请输入Docker数据目录 (默认 /var/lib/docker): " DOCKER_DIR
DOCKER_DIR=${DOCKER_DIR:-/var/lib/docker}
rm -rf $DOCKER_DIR

# 删除 iptables 规则
echo -e "${YELLOW}清理iptables规则...${NC}"
iptables -F
iptables -t nat -F
iptables -t mangle -F
iptables -X

# 卸载 Docker
echo -e "${YELLOW}卸载Docker...${NC}"
yum remove -y docker-ce docker-ce-cli containerd.io

# 删除主机名配置
echo -e "${YELLOW}恢复主机名为默认值...${NC}"
hostnamectl set-hostname localhost

# 删除 /etc/hosts 中的 Kubernetes 相关条目
echo -e "${YELLOW}清理/etc/hosts文件中的Kubernetes相关条目...${NC}"
sed -i '/k8s-master/d' /etc/hosts
sed -i '/k8s-worker/d' /etc/hosts

# 删除网络模块配置
echo -e "${YELLOW}清理网络模块配置...${NC}"
rm -f /etc/sysconfig/modules/br_netfilter.modules
rm -f /etc/sysconfig/modules/ipvs.modules
rm -f /etc/sysctl.d/k8s.conf

# 恢复 SELinux 和 swap
echo -e "${YELLOW}恢复SELinux和swap设置...${NC}"
sed -i 's#SELINUX=disabled#SELINUX=enforcing#g' /etc/sysconfig/selinux
sed -i 's#SELINUX=disabled#SELINUX=enforcing#g' /etc/selinux/config
setenforce 1

sed -ri '/^[^#]*swap/s@^#@@' /etc/fstab
swapon -a

# 提示用户检查
echo -e "${GREEN}卸载完成，请检查是否还有残留数据或配置。${NC}"