import os
import glob

def convert_encoding(input_file, output_file, input_encoding='gbk', output_encoding='utf-8'):
    try:
        with open(input_file, 'r', encoding=input_encoding) as infile:
            with open(output_file, 'w', encoding=output_encoding) as outfile:
                for line in infile:
                    outfile.write(line)
        print(f"Converted {input_file} to {output_file}")
    except Exception as e:
        print(f"Error converting {input_file}: {e}")

# 定义原始日志文件目录和转换后日志文件目录
input_dir = r'D:\kunyue\n_heater\logs'
output_dir = r'D:\kunyue\n_heater\logs_utf8'

# 检查原始日志文件目录是否存在
if not os.path.exists(input_dir):
    print(f"Input directory {input_dir} does not exist.")
else:
    # 创建转换后日志文件目录（如果不存在）
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        print(f"Created output directory {output_dir}")

    # 遍历原始日志文件目录中的所有日志文件
    for log_file in glob.glob(os.path.join(input_dir, '*.log')):
        # 定义转换后日志文件的路径
        output_file = os.path.join(output_dir, os.path.basename(log_file))
        # 进行编码转换
        convert_encoding(log_file, output_file)