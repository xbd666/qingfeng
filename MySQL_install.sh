#!/bin/bash
#本脚本用于安装MySQL，MariaDB
# author: Mr.xu
# date: 2024-12-03	

# 颜色输出函数
print_color() {
    case $1 in
        "red")    echo -e "\033[31m\033[01m$2\033[0m" ;;
        "green")  echo -e "\033[32m\033[01m$2\033[0m" ;;
        "yellow") echo -e "\033[33m\033[01m$2\033[0m" ;;
        "blue")   echo -e "\033[34m\033[01m$2\033[0m" ;;
    esac
}

# 全局变量
MYSQL_IP=$(hostname -I | awk '{print $1}')
MYSQL_ROOT_PASSWORD="Qingfeng@123"
MYSQL_DATA_DIR="/data/mysql"
MYSQL_LOG_DIR="/var/log/mysql"
MYSQL_CONFIG="/etc/my.cnf"
MYSQL_REPO="/etc/yum.repos.d/mysql.repo"

# 日志函数
log() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

# 错误检查函数
check_error() {
    if [ $? -ne 0 ]; then
        print_color "red" "错误: $1"
        exit 1
    fi
}

# MySQL安装函数
install_mysql() {
    log "开始安装MySQL..."
    
    # 系统准备
    systemctl stop firewalld
    systemctl disable firewalld 
    setenforce 0
    
    # 清理旧安装
    log "清理旧的MySQL安装..."
    systemctl stop mysqld 2>/dev/null
    yum remove -y mysql-community-server
    rm -rf /var/lib/mysql ${MYSQL_DATA_DIR} ${MYSQL_CONFIG} ${MYSQL_REPO}
    
    # 创建MySQL repo
    cat > ${MYSQL_REPO} <<EOF
[mysql]
name=MySQL 5.7 Community Server
baseurl=https://mirrors.tuna.tsinghua.edu.cn/mysql/yum/mysql-5.7-community-el7-x86_64/
gpgcheck=0
enabled=1
gpgkey=https://mirrors.ustc.edu.cn/mysql-repo/RPM-GPG-KEY-mysql
EOF
    
    # 安装MySQL
    log "安装MySQL包..."
    yum -y install mysql-community-server
    check_error "MySQL安装失败"
    
    # 创建目录
    mkdir -p ${MYSQL_DATA_DIR}/data ${MYSQL_LOG_DIR}
    chown -R mysql:mysql ${MYSQL_DATA_DIR} ${MYSQL_LOG_DIR}
    chmod 755 ${MYSQL_DATA_DIR} ${MYSQL_LOG_DIR}
    
    # 配置MySQL
    cat > ${MYSQL_CONFIG} <<EOF
[mysqld]
datadir=${MYSQL_DATA_DIR}/data
socket=/var/lib/mysql/mysql.sock
log-error=${MYSQL_LOG_DIR}/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
character-set-server=utf8mb4
collation-server=utf8mb4_general_ci

[client]
default-character-set=utf8mb4
EOF
    
    # 初始化MySQL
    log "初始化MySQL..."
    mysqld --initialize --user=mysql --datadir=${MYSQL_DATA_DIR}/data
    check_error "MySQL初始化失败"
    
    # 启动MySQL
    systemctl start mysqld
    check_error "MySQL启动失败"
    systemctl enable mysqld
    
    # 获取临时密码
    TEMP_PASSWORD=$(grep 'temporary password' ${MYSQL_LOG_DIR}/mysqld.log | awk '{print $NF}' | tail -1)
    if [ -z "${TEMP_PASSWORD}" ]; then
        print_color "red" "无法获取临时密码"
        exit 1
    fi
    
    # 配置MySQL
    log "配置MySQL..."
    mysql --connect-expired-password -uroot -p"${TEMP_PASSWORD}" <<EOF
ALTER USER 'root'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF
    check_error "MySQL配置失败"
    
    print_color "green" "MySQL安装完成！"
    print_color "blue" "MySQL连接信息："
    print_color "yellow" "地址: ${MYSQL_IP}"
    print_color "yellow" "密码: ${MYSQL_ROOT_PASSWORD}"
    print_color "yellow" "数据目录: ${MYSQL_DATA_DIR}/data"
}

# MariaDB安装函数
install_mariadb() {
    log "开始安装MariaDB..."
    
    yum -y install epel-release
    yum -y install mariadb-server mariadb
    
    systemctl start mariadb
    check_error "MariaDB启动失败"
    
    mysqladmin -u root password "${MYSQL_ROOT_PASSWORD}"
    check_error "MariaDB密码设置失败"
    
    mysql -uroot -p"${MYSQL_ROOT_PASSWORD}" <<EOF
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
FLUSH PRIVILEGES;
EOF
    check_error "MariaDB权限配置失败"
    
    print_color "green" "MariaDB安装完成！"
    print_color "yellow" "密码: ${MYSQL_ROOT_PASSWORD}"
}

# 卸载函数
uninstall_db() {
    local db_type=$1
    log "开始卸载 ${db_type}..."
    
    systemctl stop ${db_type,,}d 2>/dev/null
    
    if [ "$db_type" = "MySQL" ]; then
        yum remove -y $(rpm -qa | grep mysql)
        rm -rf /var/log/mysql* /var/lib/mysql* /usr/local/mysql* ${MYSQL_DATA_DIR}
    else
        yum remove -y $(rpm -qa | grep mariadb)
        rm -rf /var/log/mariadb* /etc/my.cnf.d* /var/lib/mysql*
    fi
    
    print_color "green" "${db_type}卸载完成"
}

# 主菜单
show_menu() {
    clear
    print_color "red"   "==============================================="
    print_color "green" "||  ____    _____  _______  _________  _______||"
    print_color "yellow" "|| / __ \  /  _/ |/ / ___/ / __/ __/ |/ / ___/||"
    print_color "blue"   "||/ /_/ / _/ //    / (_ / / _// _//    / (_ / ||"
    print_color "yellow" "||\___\_\/___/_/|_/\___/ /_/ /___/_/|_/\___/  ||"
    print_color "green"  "||                                            ||"
    print_color "red"    "==============================================="
    print_color "blue"   "||            >>>>> 清风徐来 <<<<<            ||"
    print_color "red"    "==============================================="
    print_color "blue"   "1) 安装 <  MySQL  >"
    print_color "blue"   "2) 安装 < MariaDB >"
    print_color "yellow" "3) 卸载 MySQL"
    print_color "yellow" "4) 卸载 MariaDB"
    print_color "red"    "5) 退出"
    print_color "red"    "==============================================="
}

# 主程序
main() {
    while true; do
        show_menu
        read -p "请输入选项 [1-5]: " choice
        case $choice in
            1) install_mysql ;;
            2) install_mariadb ;;
            3) uninstall_db "MySQL" ;;
            4) uninstall_db "MariaDB" ;;
            5) print_color "green" "再见！！！"; exit 0 ;;
            *) print_color "red" "无效的选择，请重新输入" ;;
        esac
        echo "============================"
        read -p "按任意键返回主菜单" any_key
        clear
    done
}

# 执行主程序
main