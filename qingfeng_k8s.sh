#!/bin/bash
# 脚本功能：安装k8s集群所需的环境
# 作者：Mr.xu
# 时间：2024-11-29

# 定义颜色
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# 询问节点角色
echo -e "${GREEN}请选择节点角色:${NC}"
echo -e "${YELLOW}1. Master 节点${NC}"
echo -e "${YELLOW}2. Worker 节点${NC}"
read -p "请输入选择 (1/2): " NODE_ROLE

case $NODE_ROLE in
    1)
        ROLE="master"
        ;;
    2)
        ROLE="worker"
        ;;
    *)
        echo -e "${RED}无效的选择，请输入 1 或 2${NC}"
        exit 1
        ;;
esac

# 定义变量，获取用户输入的IP地址和Docker目录
echo -e "${GREEN}将要输入5条信息（master、worker01、worker02、worker03、docker目录）${NC}"
read -p "请输入master的IP地址：" MASTER_IP
read -p "请输入worker01的IP地址：" WORKER01_IP
read -p "请输入worker02的IP地址：" WORKER02_IP
read -p "请输入worker03的IP地址：" WORKER03_IP
read -p "请输入docker目录：" DOCKER_DIR

# 初始化主机名变量
HOSTNAME=""
# 根据当前主机的IP地址设置主机名
case "$(hostname -I | awk '{print $1}')" in
    "$MASTER_IP")
        HOSTNAME="k8s-master"
        ;;
    "$WORKER01_IP")
        HOSTNAME="k8s-worker01"
        ;;
    "$WORKER02_IP")
        HOSTNAME="k8s-worker02"
        ;;
    "$WORKER03_IP")
        HOSTNAME="k8s-worker03"
        ;;
    *)
        echo -e "${RED}未识别的IP地址，无法设置主机名${NC}"
        exit 1
        ;;
esac

# 检查并设置主机名
current_hostname=$(hostname)
if [ "$current_hostname" != "$HOSTNAME" ]; then
    hostnamectl set-hostname $HOSTNAME
    echo -e "${GREEN}主机名已设置为 $HOSTNAME${NC}"
else
    echo -e "${YELLOW}主机名已是 $HOSTNAME，无需更改${NC}"
fi

# 定义主机条目并添加到/etc/hosts文件
add_line1=("$MASTER_IP k8s-master" "$WORKER01_IP k8s-worker01" "$WORKER02_IP k8s-worker02" "$WORKER03_IP k8s-worker03")
POD_NETWORK_CIDR="10.244.0.0/16"

# 检查并添加主机条目
for target_string in "${add_line1[@]}"; do
    if ! grep -q "${target_string}" /etc/hosts; then
        echo "${target_string}" >> /etc/hosts
        echo "已将${target_string} 添加到/etc/hosts文件。"  
    else
        echo "/etc/hosts文件中已经包含${target_string}，跳过该步骤。"  
    fi
done

# 关闭防火墙、SELinux、dnsmasq、NetworkManager和swap
systemctl is-active --quiet firewalld && systemctl disable --now firewalld 
systemctl is-active --quiet dnsmasq && systemctl disable --now dnsmasq
systemctl is-active --quiet NetworkManager && systemctl disable --now NetworkManager

# 设置SELinux为disabled
if [ "$(getenforce)" != "Disabled" ]; then
    setenforce 0
    sed -i 's#SELINUX=enforcing#SELINUX=disabled#g' /etc/sysconfig/selinux
    sed -i 's#SELINUX=enforcing#SELINUX=disabled#g' /etc/selinux/config
fi

# 关闭swap分区
swapoff -a && sysctl -w vm.swappiness=0
if ! grep -q '#/dev/mapper/centos-swap' /etc/fstab; then  
    sed -ri '/^[^#]*swap/s@^@#@' /etc/fstab
    echo "已执行关闭swap分区动作"  
else
    echo "已关闭swap分区，跳过该步骤"    
fi

# 检查并安装Docker
if ! command -v docker &> /dev/null; then
    yum remove -y docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-selinux \
    docker-engine-selinux \
    docker-engine

    yum install -y yum-utils device-mapper-persistent-data lvm2 git
    yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

    yum install -y docker-ce

    # 启动并设置Docker开机自启
    systemctl start docker && systemctl enable docker
else
    echo "Docker已安装，跳过安装步骤"
fi

# 配置Docker目录和镜像加速器
[ ! -d "/etc/docker" ] && mkdir -pv /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
"exec-opts":["native.cgroupdriver=systemd"],
"data-root": "$DOCKER_DIR",
"registry-mirrors": ["https://docker.registry.cyou",
"https://docker-cf.registry.cyou",
"https://dockercf.jsdelivr.fyi",
"https://docker.jsdelivr.fyi",
"https://dockertest.jsdelivr.fyi",
"https://mirror.aliyuncs.com",
"https://dockerproxy.com",
"https://mirror.baidubce.com",
"https://docker.m.daocloud.io",
"https://docker.nju.edu.cn",
"https://docker.mirrors.sjtug.sjtu.edu.cn",
"https://docker.mirrors.ustc.edu.cn",
"https://mirror.iscas.ac.cn",
"https://docker.rainbond.cc",
"https://registry.cn-hangzhou.aliyuncs.com",
"https://hub-mirror.c.163.com",
"https://8zc4xpn2.mirror.aliyuncs.com",
"https://docker.7boe.top"]
}
EOF

# 重新加载并重启Docker服务
systemctl daemon-reload
systemctl restart docker

# 安装命令补全工具
yum -y install bash-completion
source /etc/profile.d/bash_completion.sh

# 拉取Kubernetes镜像
images=(
    "registry.cn-hangzhou.aliyuncs.com/google_containers/kube-controller-manager:v1.22.2"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/kube-proxy:v1.22.2"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/kube-apiserver:v1.22.2"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/kube-scheduler:v1.22.2"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:1.8.4"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:3.5.0-0"
    "registry.cn-hangzhou.aliyuncs.com/google_containers/pause:3.5"
)

# 拉取并标记镜像
for image in "${images[@]}"; do
    docker pull ${image}
    # 提取镜像名称和版本
    image_name=$(echo ${image} | awk -F'/' '{print $NF}')
    # 标记镜像
    docker tag ${image} k8s.gcr.io/${image_name}
    # 删除原始镜像
    docker rmi ${image}
done

# 加载br_netfilter模块
lsmod | grep br_netfilter || modprobe br_netfilter

# 创建br_netfilter模块加载脚本
cat > /etc/sysconfig/modules/br_netfilter.modules <<EOF
#!/bin/bash
modprobe br_netfilter
EOF

chmod 755 /etc/sysconfig/modules/br_netfilter.modules

# 修改内核参数
cat > /etc/sysctl.d/k8s.conf <<EOF
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
EOF

# 应用内核参数
sysctl -p /etc/sysctl.d/k8s.conf

# 安装ipvs相关工具
yum install -y ipset ipvsadm

# 加载ipvs模块
cat > /etc/sysconfig/modules/ipvs.modules <<EOF
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
EOF

chmod 755 /etc/sysconfig/modules/ipvs.modules && bash /etc/sysconfig/modules/ipvs.modules && lsmod | grep -e ip_vs -e nf_conntrack_ipv4

# 设置kubernetes源
cat > /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

# 更换K8S的镜像源
kubeadm config images pull --image-repository registry.aliyuncs.com/google_containers

# 检查并安装kubelet、kubeadm和kubectl
if ! command -v kubelet &> /dev/null; then
    yum list kubelet --showduplicates | sort -r
    yum install -y kubelet-1.22.2 kubeadm-1.22.2 kubectl-1.22.2
else
    echo "Kubernetes组件已安装，跳过安装步骤"
fi

# 启用kubectl命令补全
echo "source <(kubectl completion bash)" >> ~/.bashrc && source ~/.bashrc

# 启动kubelet服务
systemctl start kubelet.service && systemctl enable kubelet.service
kubeadm config images pull --image-repository registry.aliyuncs.com/google_containers
# Master节点特有的配置
if [ "$ROLE" == "master" ]; then
    echo -e "${GREEN}开始配置Master节点...${NC}"
    
    # 检查是否已经初始化过Kubernetes集群
    if [ ! -f /etc/kubernetes/admin.conf ]; then
        # 初始化Kubernetes集群
        echo -e "${YELLOW}正在初始化Kubernetes集群...${NC}"
        kubeadm init --kubernetes-version=v1.22.2 \
                     --pod-network-cidr=10.244.0.0/16 \
                     --apiserver-advertise-address=$MASTER_IP \
                     --pod-network-cidr=$POD_NETWORK_CIDR \
                     --ignore-preflight-errors=Swap | tee kubeadm-init.log

        # 设置kubectl配置
        mkdir -p $HOME/.kube
        cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
        chown $(id -u):$(id -g) $HOME/.kube/config

        # 保存join命令
        echo -e "${GREEN}保存join命令到join-command.sh${NC}"
        grep -A 1 "kubeadm join" kubeadm-init.log > join-command.sh
        chmod +x join-command.sh

        # 提示部署网络插件
        echo -e "${YELLOW}请部署Flannel网络插件：${NC}"
        echo "curl -O https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml"
        echo -e "${YELLOW}编辑kube-flannel.yml，在args下添加：${NC}"
        echo "        - operator: Exists"
        echo "          effect: NoSchedule"
        echo "        - key: node.kubernetes.io/not-ready"
        echo "          operator: Exists"
        echo "          effect: NoSchedule"
        echo "-------------分割线----------------"
        echo "        - --kube-subnet-mgr"
        echo "        - --iface=ens192"
    else
        echo -e "${YELLOW}Kubernetes集群已初始化，跳过初始化步骤${NC}"
    fi
else
    echo -e "${GREEN}开始配置Worker节点...${NC}"
    
    # Worker节点需要等待join命令
    echo -e "${YELLOW}请在master节点获取join命令并在此执行${NC}"
    echo -e "${YELLOW}或者执行已保存的join-command.sh脚本${NC}"
fi

# 最后的提示信息
if [ "$ROLE" == "master" ]; then
    echo -e "${GREEN}Master节点配置完成！${NC}"
    echo -e "${YELLOW}请记得：${NC}"
    echo -e "1. 部署网络插件"
    echo -e "2. 保存join命令用于Worker节点加入"
else
    echo -e "${GREEN}Worker节点配置完成！${NC}"
    echo -e "${YELLOW}请执行join命令加入集群${NC}"
fi