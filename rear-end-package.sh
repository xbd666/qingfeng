#!/bin/bash
#本脚本用于安装上线软件包
#tomcat,jdk,maven,git
#author: qingfeng
#creation_time:2024-1-1

#定义变量
PASS_WORD="zabbix"
mysql_db="zabbix"
GIT_PASSWD="Zj123456"
GIT_NAME="git"

#安装zabbix
INSTALL_zabbix(){
    read -p "请选择要安装的软件：1.zabbix-server 2.zabbix-agent:" num
    case $num in
        1)
            #关闭防火墙
            systemctl stop firewalld
            setenforce 0
            sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
            #下载zabbix-5.0的rpmbao
            rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
            # 更新yum源
            yum clean all
            yum repolist
            #替换阿里的源
            sed -i 's#http://repo.zabbix.com#https://mirrors.aliyun.com/zabbix#' /etc/yum.repos.d/zabbix.repo
            #安装
            yum -y install zabbix-server-mysql zabbix-web-mysql zabbix-agent
            yum -y remove zabbix6.0-6.0.25-1.el7.x86_64
            yum -y install zabbix-get.x86_64
            #安装mariadb数据库
            yum install -y mariadb mariadb-server
            systemctl restart mariadb && systemctl enable mariadb
            #设置数据库密码
            mysqladmin -uroot password "$PASS_WORD"
            #创建数据库，并授权 用
            mysql -uroot -p"$PASS_WORD" <<EOF
create database zabbix character set utf8 collate utf8_bin;
grant all privileges on zabbix.* to 'zabbix'@'localhost' identified by "$PASS_WORD";
flush privileges;
EOF
            #导入表
            zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p"$PASS_WORD" "$mysql_db"
            # 配置server段--修改server端的配置文件
            cp /etc/zabbix/zabbix_server.conf /etc/zabbix/zabbix_server.conf.bak
            sed -i "s/# DBHost=.*/DBHost=localhost/" /etc/zabbix/zabbix_server.conf
            sed -i "s/# DBPassword=.*/DBPassword=zabbix/" /etc/zabbix/zabbix_server.conf
            #启动
            systemctl start zabbix-server && systemctl enable zabbix-server
            sed -i "11s/enabled=0/enabled=1/" /etc/yum.repos.d/zabbix.repo
            yum install centos-release-scl -y
            yum install zabbix-web-mysql-scl zabbix-apache-conf-scl -y
            yum install -y httpd rh-php72-php-fpm
            echo 'php_value[date.timezone] = Asia/Shanghai' >> /etc/opt/rh/rh-php72/php-fpm.d/zabbix.conf
            systemctl start httpd rh-php72-php-fpm
            systemctl restart zabbix-server zabbix-agent httpd rh-php72-php-fpm
            systemctl enable zabbix-server zabbix-agent httpd rh-php72-php-fpm
            green "访问地址：本机的IP/zabbix"
            green "默认用户名：Admin   密码：是在web上设置的密码"
        ;;
        2)
            #关闭防火墙
            systemctl stop firewalld
            setenforce 0
            sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config 
            #安装zabbix--agent端
            rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
            yum install zabbix-agent zabbix-sender -y
            cp /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf.bak
            read -p "请你输入zabbix——server端的IP地址:" zabbix_agent_IP
            sed -i "s/Server=.*/Server=$zabbix_agent_IP/" /etc/zabbix/zabbix_agentd.conf
            read -p "请你输入zabbix——ServerActive端的IP地址:" zabbix_agent_ip
            sed -i "s/ServerActive=.*/ServerActive=$zabbix_agent_ip/" /etc/zabbix/zabbix_agentd.conf
            read -p "请你自定义zabbix——Hostname的名字:" zabbix_agent_name
            sed -i "s/Hostname=.*/Hostname=$zabbix_agent_name/" /etc/zabbix/zabbix_agentd.conf
            sed -i "s/# UnsafeUserParameters=0/UnsafeUserParameters=1/" /etc/zabbix/zabbix_agentd.conf
            systemctl start zabbix-agent && systemctl enable zabbix-agent
            green "zabbix-agent端部署完成，请前往web界面操作！"
        ;;
        *)
            echo "无效的选择"
        ;;
    esac
}

#安装jdk/maven/tomcat
INSTALL_jdk(){
    read -p "请选择要安装的软件：1.jdk-11 2.jdk-1.8 3.maven-3.9.6 4.maven-3.9.3 5.tomcat9 :" choice
    case $choice in
        1)  
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/tomcat-jdk/jdk-11.0.20_linux-x64_bin.tar.gz
            tar zxvf jdk-11.0.20_linux-x64_bin.tar.gz -C /usr/local
            mv /usr/local/jdk-11.0.20 /usr/local/java
            echo 'JAVA_HOME=/usr/local/java' >> /etc/profile
            echo 'PATH=$JAVA_HOME/bin:$PATH' >> /etc/profile
            echo 'export JAVA_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "jdk-11.0.20--安装目录/usr/local/java，未启动"

            ;;
        2)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/tomcat-jdk/jdk-8u271-linux-x64.tar.gz
            tar zxvf jdk-8u271-linux-x64.tar.gz -C /usr/local
            mv /usr/local/jdk1.8.0_271 /usr/local/java
            echo 'JAVA_HOME=/usr/local/java' >> /etc/profile
            echo 'PATH=$JAVA_HOME/bin:$PATH' >> /etc/profile
            echo 'export JAVA_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "jdk1.8.0_271--安装目录/usr/local/java，未启动"
            ;;
        3)  
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/maven-nodejs/apache-maven-3.9.6-bin.tar.gz
            tar zxvf apache-maven-3.9.6-bin.tar.gz -C /usr/local
            mv /usr/local/apache-maven-3.9.6 /usr/local/maven
            echo 'MAVEN_HOME=/usr/local/maven' >> /etc/profile
            echo 'PATH=$MAVEN_HOME/bin:$PATH' >> /etc/profile
            echo 'export MAVEN_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "maven-3.9.6--安装目录/usr/local/maven，未启动"
            ;;
        4)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/maven-nodejs/apache-maven-3.9.3-bin.tar.gz
            tar zxvf apache-maven-3.9.3-bin.tar.gz -C /usr/local
            mv /usr/local/apache-maven-3.9.3 /usr/local/maven
            echo 'MAVEN_HOME=/usr/local/maven' >> /etc/profile
            echo 'PATH=$MAVEN_HOME/bin:$PATH' >> /etc/profile
            echo 'export MAVEN_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "maven-3.9.3--安装目录/usr/local/maven，未启动"            
            ;;
        5)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/tomcat-jdk/apache-tomcat-9.0.83.tar.gz
            tar zxvf apache-tomcat-9.0.83.tar.gz -C /usr/local
            mv /usr/local/apache-tomcat-9.0.83 /usr/local/tomcat
            red "启动 tomcat命令--/usr/local/tomcat/bin/startup.sh"
            red "关闭 tomcat命令--/usr/local/tomcat/bin/shutdown.sh"
            green "查看日志 tail -f /usr/local/tomcat/logs/catalina.out"
            green "tomcat-9.0.83 发布目录 /usr/local/tomcat/webapps"
            ;;
        *)
            echo "无效的选择"
            ;;
    esac
}


#下载git/php
INSTALL_git(){
    read -p "请选择要安装的软件：1.git 2.php :" who
    case $who in
        1)
            yum install -y git
            #设置邮箱
            git config --global user.email "soho@163.com"
            #添加用户 
            git config --global user.name "soho"
            #创建一个空目录：在中心服务器上创建
            mkdir /git-test
            #创建一个git用户用来运行git
            useradd "$GIT_NAME"
            #给用户设置密码
            echo "$GIT_NAME:$GIT_PASSWD" | sudo chpasswd
            cd /git-test/
            #创建一裸库
            git init --bare testgit
            #修改权限
            chown git.git /git-test -R
            green "已为用户 $GIT_NAME ，设置密码为 $GIT_PASSWD"
            green "请开始密钥的操作，生成密钥后就可以 克隆仓库了！"
            ;;
        2)
            rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
            rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
            yum install php71w-xsl php71w php71w-xsl php71w php71w-ldap php71w-cli php71w-common php71w-devel php71w-gd php71w-pdo php71w-mysql php71w-mbstring php71w-bcmath php71w-mcrypt
            #下载主包
            yum install -y php71w-fpm
            # 启动
            systemctl start php-fpm
            #修改配置文件
            cat >> /etc/nginx/conf.d/php.conf <<EOF
server {
    listen      80;
    server_name     localhost;
    location ~ \.php$ {
        root       /usr/share/nginx/html;  #指定网站目录
        fastcgi_pass   127.0.0.1:9000;    #开启fastcgi连接php地址
        fastcgi_index  index.php;       #指定默认文件
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name; #站点根目录，取决于root配置项
        include        fastcgi_params;  #包含fastcgi使用的常量
    }
}
EOF
            # 创建php页面
            cat >> /usr/share/nginx/html/test.php <<EOF
<?php
 phpinfo();
?>
EOF
            # 重启nginx
            systemctl restart nginx
            green "在/usr/share/nginx/html/目录下创建了test.php的php页面代码"
            green "输入 本机ip/test.php  出来php页面"
            ;;
        *)
            echo "无效的选择"
            ;;
    esac
}

#下载redis-4.0.9.tar.gz/下载redis-7.0.9.tar.gz
INSTALL_redis(){
    read -p "请选择要安装的软件：1.redis-7.9 2.redis-4 :" who
    case $who in
        1)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/redis/redis-7.0.9.tar.gz
            tar xzvf redis-7.0.9.tar.gz -C /usr/local/
            mv /usr/local/redis-7.0.9 /usr/local/redis
            yum install -y gcc make
            cd /usr/local/redis
            make
            cp /usr/local/redis/redis.conf /usr/local/redis/redis.conf.bak
            green "请修改 redis.conf 配置文件--修改以下内容：
            bind 192.168.246.202　　#只监听内网IP
            daemonize yes　　　　　#开启后台模式将no改为yes
            port 6379                      #端口号
            dir /data/application/redis/data　　#本地数据库存放持久化数据的目录该目录-----需要存在
            requirepass 1122334    #设置密码
            logfile '/var/log/redis.log'  #设置日志存放路径与日志名"
            red "redis 后台启动命令-- nohup /usr/local/redis/src/redis-server redis.conf &"
            red "redis-7.0.9--安装目录/usr/local/redis，未启动"
            red "redis-配置文件目录/usr/local/redis/redis.conf，需进行修改"
        ;;
        2)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/redis/redis-4.0.9.tar.gz
            tar xzvf redis-4.0.9.tar.gz -C /usr/local/
            mv /usr/local/redis-4.0.9 /usr/local/redis
            yum install -y gcc make
            cd /usr/local/redis
            make
            cp /usr/local/redis/redis.conf /usr/local/redis/redis.conf.bak
            green "请修改 redis.conf 配置文件--修改以下内容：
            bind 192.168.246.202　　#只监听内网IP
            daemonize yes　　　　　#开启后台模式将no改为yes
            port 6379                      #端口号
            dir /data/application/redis/data　　#本地数据库存放持久化数据的目录该目录-----需要存在
            requirepass 1122334    #设置密码
            logfile '/var/log/redis.log'  #设置日志存放路径与日志名"
            red "redis 后台启动命令-- nohup /usr/local/redis/src/redis-server redis.conf &"
            red "redis-4.0.9--安装目录/usr/local/redis，未启动"
        ;;
        *)
            echo "无效的选择"
        ;;        
    esac
}

#下载rabbitmq-server-3.7.10-1.el7.noarch.rpm
#下载erlang-21.3.8.21-1.el7.x86_64.rpm
INSTALL_rabbitmq-erlang(){
    #启动rabbitmq需要更改主机名
    read -p "请输入需要更改的hostname：" name
    hostnamectl set-hostname $name
    #安装erlang依赖
    yum install -y *epel* gcc-c++ unixODBC unixODBC-devel openssl-devel ncurses-devel
    #下载erlang 
    wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/rabbitmq-erlang/erlang-21.3.8.21-1.el7.x86_64.rpm
    yum -y install erlang-21.3.8.21-1.el7.x86_64.rpm
    #下载rabbitmq
    wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/rabbitmq-erlang/rabbitmq-server-3.7.10-1.el7.noarch.rpm
    yum install -y rabbitmq-server-3.7.10-1.el7.noarch.rpm
    #启动
    systemctl daemon-reload
    systemctl start rabbitmq-server && systemctl enable rabbitmq-server
    #开启rabbitmq的web访问界面
    rabbitmq-plugins enable rabbitmq_management
    #添加用户和密码
    #rabbitmqctl add_user soho soso
    #设置为管理员
    #rabbitmqctl set_user_tags soho administrator
    #此处设置权限时注意'.*'之间需要有空格 三个'.*'分别代表了conf权限，read权限与write权限，soho设置这三个权限前是没有权限查询队列，在ui界面也看不见
    #rabbitmqctl set_permissions -p "/" soho ".*" ".*" ".*"
    #red "设置了soho用户，密码soso，并授予权限。。。"
    #所有机器都操作:开启用户远程登录
    cp /usr/share/doc/rabbitmq-server-3.7.10/rabbitmq.config.example /etc/rabbitmq/rabbitmq.config
    sed -i '61s/%% {loopback_users, \[\]},/{loopback_users, \[\]}/' /etc/rabbitmq/rabbitmq.config
    systemctl restart rabbitmq-server
    red "RabbitMQ 默认使用端口5672，以及Erlang端口4369和25672。"
    green "web访问master主机的 IP:15672"
    green "rabbitmq默认管理员用户:guest   密码:guest"
}

#下载node-v14.15.3-linux-x64.tar.gz
#安装node.js前端打包工具命令npm
INSTALL_node(){
    read -p "请选择要安装的软件：1.XZ(v12.18.4) 2.gz(v14.15.3) :" choice
    case $choice in
        1)  
            wget https://nodejs.org/dist/v12.18.4/node-v12.18.4-linux-x64.tar.xz
            tar xf node-v12.18.4-linux-x64.tar.xz -C /usr/local/
            mv /usr/local/node-v12.18.4-linux-x64 /usr/local/node
            echo 'NODE_HOME=/usr/local/node' >> /etc/profile
            echo 'PATH=$NODE_HOME/bin:$PATH' >> /etc/profile
            echo 'export NODE_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "node-v12.18.4 安装成功--前端打包工具命令npm"
            red "前端打包命令--在RuoYi-Vue/ruoyi-ui/目录下运行 npm run build:prod命令"
            red "node-v12.18.4--安装目录/usr/local/node，未启动"
            ;;
        2)
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/maven-nodejs/node-v14.15.3-linux-x64.tar.gz
            tar zxvf node-v14.15.3-linux-x64.tar.gz -C /usr/local/
            mv /usr/local/node-v14.15.3-linux-x64 /usr/local/node
            echo 'NODE_HOME=/usr/local/node' >> /etc/profile
            echo 'PATH=$NODE_HOME/bin:$PATH' >> /etc/profile
            echo 'export NODE_HOME PATH' >> /etc/profile
            cd /etc/profile
            source /etc/profile
            red "node-v14.15.3 安装成功--前端打包工具命令npm"
            red "前端打包命令--在RuoYi-Vue/ruoyi-ui/目录下运行 npm run build:prod命令"
            red "node-v14.15.3--安装目录/usr/local/node，未启动"
            ;;
        *)
            echo "无效的选择"
            ;;
    esac
}

#下载jenkins.war
INSTALL_jenkins(){
    read -p "请选择要安装的软件：1.jenkins 2.RuoYi :" numbb
    case $numbb in
        1)  
            wget https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/tomcat-jdk/jenkins.war
            red "jenkins 已下载在root目录下"
            ;;
        2)
            wet https://a.xbd666.cn/d/Aliyun/Cloud_computing/Software_package/maven-nodejs/RuoYi-Vue-master.zip
            yum -y install unzip
            unzip RuoYi-Vue-master.zip
            red "ruoyi 已下载及解压在root目录下"
            ;;
        *)
            echo "无效的选择"
            ;;
    esac
}

#退出
exit_menu(){
    green "再见！！！"
    break
}


red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}
green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}
blue(){
    echo -e "\033[34m\033[01m$1\033[0m"
}

# 主菜单选择
main_menu() {
    while true; do
red      "==============================================="
green   "||  ____    _____  _______  _________  _______||"
yellow  "|| / __ \  /  _/ |/ / ___/ / __/ __/ |/ / ___/||"
blue    "||/ /_/ / _/ //    / (_ / / _// _//    / (_ / ||"
yellow  "||\___\_\/___/_/|_/\___/ /_/ /___/_/|_/\___/  ||"
green   "||                                            ||"
red     "==============================================="
blue    "||            >>>>> 清风徐来 <<<<<            ||"
red     "==============================================="
        echo "1) 下载 zabbix"
        echo "2) 下载 jdk--maven--tomcat"
        echo "3) 下载 git--php71w"
        echo "4) 下载 redis"
        echo "5) 下载 rabbitmq-3.7.10-1--erlang-21.3.8.21-1"
        echo "6) 下载 node-XZ(v12.18.4)--node-gz(v14.15.3)"
        echo "7) 下载 jenkins(2.424.1)--RuoYi-Vue-master.zip"
        echo "8) 退出"
        echo "============================================"

read -p "请输入选项： " select
        case $select in
            1)
                INSTALL_zabbix
                ;;
            2)
                INSTALL_jdk
                ;;
            3)
                INSTALL_git
                ;;
            4)
                INSTALL_redis
                ;;
            5)
                INSTALL_rabbitmq-erlang
                ;;
            6)
                INSTALL_node
                ;;
            7)
                INSTALL_jenkins
                ;;
            8)
                exit_menu
                ;;
            *)
                red "无效的选择，请重新输入"
                continue 2
                ;;
        esac

        echo "============================"
        read -p "按任意键返回主菜单" any_key
        clear
    done
}

#运行脚本
main_menu