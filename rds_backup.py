import requests
import json
from datetime import datetime, timezone, timedelta
import os

# 华为云配置
project_id = '087c642d510026892f4ac00b3ec73471'
region = 'cn-east-3'
domain_name = 'a-INECO'
username = 'xubaodong'
password = 'Kunlun@123'
endpoint = f'https://rds.{region}.myhuaweicloud.com'

# 获取登入华为云的token
def get_token():
    auth_url = f"https://iam.{region}.myhuaweicloud.com/v3/auth/tokens"
    headers = {'Content-Type': 'application/json'}
    data = {
        "auth": {
            "identity": {
                "methods": ["password"],
                "password": {
                    "user": {
                        "domain": {"name": domain_name},
                        "name": username,
                        "password": password
                    }
                }
            },
            "scope": {"project": {"id": project_id}}
        }
    }
    response = requests.post(auth_url, headers=headers, data=json.dumps(data))
    if response.status_code == 201:
        token = response.headers['X-Subject-Token']
        print("Token获取成功")
        return token
    else:
        print("获取Token失败:", response.status_code, response.text)
        return None

# 获取 RDS实例ID
def list_rds_instances(token):
    url = f'{endpoint}/v3/{project_id}/instances'
    headers = {'X-Auth-Token': token}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        instances = response.json().get('instances', [])
        for instance in instances:
            print(f"RDS实例ID: {instance['id']} 名称: {instance['name']}")
        return instances
    else:
        print("获取RDS实例列表失败:", response.status_code, response.text)
        return None
        
# 获取当天的全量备份文件
def get_full_backup_list(token, instance_id):
    headers = {'X-Auth-Token': token}
    url = f'{endpoint}/v3/{project_id}/backups?instance_id={instance_id}'
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        backups = response.json().get('backups', [])
        latest_full_backup = None

        for backup in backups:
            backup_type = backup.get("type")
            begin_time_str = backup.get('begin_time', None)
            
            # 检查是否是全量备份并且有开始时间
            if backup_type == 'auto' and begin_time_str:
                if not latest_full_backup:
                    latest_full_backup = backup
                else:
                    # 比较开始时间，找到最新的全量备份
                    current_begin_time = datetime.strptime(backup['begin_time'], "%Y-%m-%dT%H:%M:%S%z")
                    latest_begin_time = datetime.strptime(latest_full_backup['begin_time'], "%Y-%m-%dT%H:%M:%S%z")
                    
                    if current_begin_time > latest_begin_time:
                        latest_full_backup = backup
        
        if latest_full_backup:
            print(f"最新的全量备份: {json.dumps(latest_full_backup, indent=2)}")
        else:
            print("没有找到全量备份。")
        return latest_full_backup
    else:
        print("获取备份列表失败:", response.status_code, response.text)
        return None

# 获取当天的备份文件链接
def get_download_link(token, backup_id):
    url = f'{endpoint}/v3/{project_id}/backup-files?backup_id={backup_id}'
    headers = {'X-Auth-Token': token}
    
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        download_info = response.json()
        files = download_info.get('files', [])
        if files:
            download_link = files[0].get('download_link')
            if download_link:
                print(f"下载链接: {download_link}")
            else:
                print("文件中没有下载链接。")
            return download_link
        else:
            print("没有找到文件。")
        return None
    else:
        print("获取下载链接失败:", response.status_code, response.text)
        return None

# 下载文件
def download_backup(download_link, destination_path):
    response = requests.get(download_link, stream=True)
    if response.status_code == 200:
        # 获取文件名并确保完整路径正确
        file_name = download_link.split('/')[-1].split('?')[0]
        full_path = os.path.join(destination_path, file_name)
        
        with open(full_path, 'wb') as f:
            for chunk in response.iter_content(chunk_size=8192):
                f.write(chunk)
        print(f"备份下载成功: {full_path}")
    else:
        print("下载备份失败:", response.status_code, response.text)

# 删除7天前的文件
def delete_old_backups(destination_path, days=7):
    """删除指定文件夹中，修改时间在7天之前的文件。"""
    cutoff_time = datetime.now(timezone.utc) - timedelta(days=days)
    
    print("开始删除7天之前的备份文件...")
    
    for filename in os.listdir(destination_path):
        full_path = os.path.join(destination_path, filename)
        if os.path.isfile(full_path):
            file_mod_time = datetime.fromtimestamp(os.path.getmtime(full_path), timezone.utc)
            if file_mod_time < cutoff_time:
                os.remove(full_path)
                print(f"已删除旧备份文件: {full_path}")
    
    print("旧备份文件删除完成。")

def main():
    token = get_token()
    if token:
        instances = list_rds_instances(token)
        if instances:
            instance_id = instances[0]['id']  # 使用第一个实例
            latest_backup = get_full_backup_list(token, instance_id)

            if latest_backup:
                download_link = get_download_link(token, latest_backup['id'])
                if download_link:
                    destination_path = '/home/rds/'
                    os.makedirs(destination_path, exist_ok=True)  # 确保目录存在
                    download_backup(download_link, destination_path)
                    delete_old_backups(destination_path)  # 删除过期的备份文件
                else:
                    print("未获取到下载链接。")
            else:
                print("未找到合适的备份。")
        else:
            print("无法获取RDS实例信息。")
    else:
        print("无法获取API令牌。")

if __name__ == "__main__":
    main()
